
class Function:
	def __init__(self, bblist):
		self.bblist = bblist

class Edge:
	def __init__(self, bb1, bb2, type):
		self.bb1 = bb1
		self.bb2 = bb2
		self.type = type

class BB:
	def __init__(self, start, end, insndict):
		self.start = start
		self.end = end
		self.insndict = insndict
	def setv(self, v):
		self.v = v
	def getv(self):
		return self.v
