
import sys
import importlib
import pynauty

def nauty_normal(bbset, edgelist):
	for i, bb in enumerate(bbset):
		bb.setv(i)
	adjacency_dict = {v.getv(): [e.bb1.getv() for e in edgelist if e.bb2 == v] for v in bbset}
	G = pynauty.Graph(len(bbset), directed=True, adjacency_dict=adjacency_dict)
#	print 'computing certificate of', len(bbset)
	gl = pynauty.certificate(G)
        print 'computing certificate done. cert len:', len(gl), 'vertexes:', len(bbset), 'cert entries per vertex:', len(gl)/len(bbset)
	return gl

def main():
	nucleusdata = importlib.import_module(sys.argv[1])

	print 'total bbs', len(nucleusdata.bbs)

	allbbs = {nucleusdata.bbs[x] for x in nucleusdata.bbs}
	allbbsfound=set()

	for f in nucleusdata.functions:
		functionbbs = set(f.bblist)
		refound = functionbbs & allbbsfound

		if len(refound) > 0:
			print 'found again:', [hex(bb.start) for bb in refound]

		allbbsfound |= functionbbs
		functionedges = [e for e in nucleusdata.edges if e.bb1 in functionbbs and e.bb2 in functionbbs]

		if len(functionbbs) > 20:
			# print 'skipping bbset of', len(functionbbs)
			continue

		a = nauty_normal(functionbbs, functionedges)

#		print 'function with', len(functionbbs), 'bbs and', len(functionedges), 'edges. normal:' #, a

	print 'all bbs:', len(allbbs)
	print 'found bbs:', len(allbbsfound)
	print 'BBs not in any function:', len(allbbs - allbbsfound)

main()
