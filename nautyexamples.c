/***********************************************************

Copyright Derrick Stolee 2012.

 This file is part of the Computational Combinatorics blog code examples.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    These code examples are distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SearchLib.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************/

/**
 * nautyexamples.c
 *
 * To compile with GCC, place this file in the nauty directory and use 
 * the command:
 *
 * gcc -o nautyexamples nautyexamples.c nauty.o nausparse.o gtools.o nautinv.o nautil.o -lm
 *
 *  Created on: September 20, 2012
 *      Author: Derrick Stolee
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "gtools.h"
#include "nauty.h"
#include "nausparse.h"

/** To make this an executable, define USE_MAIN */
#define USE_MAIN

/****** DEFINITIONS ********/

/**
 * outputCanonicalLabeling takes a graph and writes 
 * the canonical string to standard out.
 *
 * @param g the graph as a sparsegraph.
 * @param directed 0 if undirected, nonzero if directed.
 */
void outputCanonicalLabeling(sparsegraph* g, int directed);


/**
 * outputVColoredCanonicalLabeling takes a vertex-colored 
 * graph and writes the canonical string to standard out.
 *
 * @param g the graph as a sparsegraph.
 * @param directed 0 if undirected, nonzero if directed.
 * @param k the number of colors used (should be 0...k-1).
 * @param c the colors. c[i] should store the color of the ith vertex.
 */
void outputVColoredCanonicalLabeling(sparsegraph* g, int directed, int k, int* c);


/**
 * outputEColoredCanonicalLabeling takes an edge-colored 
 * graph and writes the canonical string to standard out.
 *
 * @param g the graph as a sparsegraph.
 * @param directed 0 if undirected, nonzero if directed.
 * @param binary 0 to use k-layer graph, nonzero to use binary layer graph. 
 * @param k the number of colors used (should be 0...k-1).
 * @param c the colors. c[i] should store the color of the pair ab with co-lex index i.
 */
void outputEColoredCanonicalLabeling(sparsegraph* g, int directed, int binary,  int k, int* c);


/**
 * setColoredPartition sets up the nauty data structures lab & ptn according to a 
 * given vertex coloring. After the execution, lab & ptn will be ready to send to nauty.
 *
 * @param g the graph as a sparsegraph.
 * @param k the number of colors.
 * @param c the colors as in outputEColoredCanonicalLabeling.
 * @param lab an integer array of at least g->nv positions. WILL BE MODIFIED.
 * @param ptn an integer array of at least g->nv positions. WILL BE MODIFIED.
 */
void setColoredPartition(sparsegraph* g, int k, int* c, int* lab, int* ptn);

/**
 * getKLayerGraph builds a k-layered graph and sets up the nauty data structures 
 * lab & ptn according to a given edge coloring. After the execution, lab & ptn 
 * will be ready to send to nauty. The resulting sparsegraph must be cleaned and
 * deleted with 'free()' by the caller. Further, the layers of the returned graph
 * are presented as consecutive copies of the vertices of g (layer 0, layer 1, ...).
 *
 * @param g the graph as a sparsegraph.
 * @param k the number of colors.
 * @param c the colors as in outputEColoredCanonicalLabeling.
 * @param lab an integer array of at least k * g->nv positions. WILL BE MODIFIED.
 * @param ptn an integer array of at least k * g->nv positions. WILL BE MODIFIED.
 * @return a new sparsegraph to send to nauty. Stores the layered graph.
 */
sparsegraph* getKLayerGraph(sparsegraph* g, int k, int* c, int* lab, int* ptn);

/**
 * getBinaryLayerGraph builds a binary layered graph and sets up the nauty data structures 
 * lab & ptn according to a given edge coloring. After the execution, lab & ptn 
 * will be ready to send to nauty. The resulting sparsegraph must be cleaned and
 * deleted with 'free()' by the caller. Further, the layers of the returned graph
 * are presented as consecutive copies of the vertices of g (layer 0, layer 1, ...).
 *
 * @param g the graph as a sparsegraph.
 * @param k the number of colors.
 * @param c the colors as in outputEColoredCanonicalLabeling.
 * @param lab an integer array of at least log(k+1) * g->nv positions. WILL BE MODIFIED.
 * @param ptn an integer array of at least log(k+1) * g->nv positions. WILL BE MODIFIED.
 * @return a new sparsegraph to send to nauty. Stores the layered graph.
 */
sparsegraph* getBinaryLayerGraph(sparsegraph* g, int k, int* c, int* lab, int* ptn);


/**
 * indexOf
 *
 * The co-lex index of the pair (i,j).
 */
int indexOf(int i, int j);




/****** IMPLEMENTATIONS ********/


/**
 * outputCanonicalLabeling takes a graph and writes 
 * the canonical string to standard out.
 *
 * @param g the graph as a sparsegraph.
 * @param directed 0 if undirected, nonzero if directed.
 */
void outputCanonicalLabeling(sparsegraph* g, int directed)
{
    int nv = g->nv;
    int m = (nv + WORDSIZE - 1) / WORDSIZE;
    nauty_check(WORDSIZE, m, nv, NAUTYVERSIONID);
    
    /* initialize helpful arrays */
    DYNALLSTAT(int, lab, lab_n);
    DYNALLSTAT(int, ptn, ptn_n);
    DYNALLSTAT(int, orbits, orbits_n);
    DYNALLOC1(int, lab, lab_n, nv, "malloc");
    DYNALLOC1(int, ptn, ptn_n, nv, "malloc");
    DYNALLOC1(int, orbits, orbits_n, nv, "malloc");
    
    /* set the options */
    static DEFAULTOPTIONS_SPARSEGRAPH( options);
    options.defaultptn = TRUE; /* Don't need colors */
    options.getcanon = TRUE; /* gets labels */
    
    if ( directed == 0 )
    {
      options.digraph = FALSE;
    }
    else
    {
      options.digraph = TRUE;
    }
    
    statsblk stats; /* we'll use this at the end */
    
    /* create the workspace for nauty to use */
    DYNALLSTAT(setword, workspace, worksize);
    DYNALLOC1(setword, workspace, worksize, 50 * m, "malloc");

    sparsegraph canon_g;
    SG_INIT(canon_g);
    
    nauty((graph*) g, lab, ptn, NULL, orbits, &options, &stats, workspace, 50 * m, m, nv, (graph*) &canon_g);

    /* THIS IS VERY IMPORTANT FOR CANONICAL STRING */
    sortlists_sg(&canon_g);
    
    char* canon_str = (char*)sgtos6(&canon_g);
    int canon_len = strlen(canon_str);
  
    printf("%s", canon_str);
    
    /* clean up */
    DYNFREE(workspace, worksize);
    DYNFREE(lab,lab_n);
    DYNFREE(ptn,ptn_n);
    DYNFREE(orbits,orbits_n);
    
    SG_FREE(canon_g);
}



/**
 * outputVColoredCanonicalLabeling takes a vertex-colored 
 * graph and writes the canonical string to standard out.
 *
 * @param g the graph as a sparsegraph.
 * @param directed 0 if undirected, nonzero if directed.
 * @param k the number of colors used (should be 0...k-1).
 * @param c the colors. c[i] should store the color of the ith vertex.
 */
void outputVColoredCanonicalLabeling(sparsegraph* g, int directed, int k, int* c)
{
    int nv = g->nv;
    int m = (nv + WORDSIZE - 1) / WORDSIZE;
    nauty_check(WORDSIZE, m, nv, NAUTYVERSIONID);
    
    /* initialize helpful arrays */
    DYNALLSTAT(int, lab, lab_n);
    DYNALLSTAT(int, ptn, ptn_n);
    DYNALLSTAT(int, orbits, orbits_n);
    DYNALLOC1(int, lab, lab_n, nv, "malloc");
    DYNALLOC1(int, ptn, ptn_n, nv, "malloc");
    DYNALLOC1(int, orbits, orbits_n, nv, "malloc");
    
    /* Setup of lab and ptn is in another method */
    setColoredPartition(g, k, c, lab, ptn); /* THIS IS CHANGED FROM outputCanonicalLabeling */
    
    /* set the options */
    static DEFAULTOPTIONS_SPARSEGRAPH( options);
    options.defaultptn = FALSE; /* WE NEED THE COLORS!  THIS IS ALSO CHANGED. */
    options.getcanon = TRUE; /* gets labels */
    
    if ( directed == 0 )
    {
      options.digraph = FALSE;
    }
    else
    {
      options.digraph = TRUE;
    }
    
    statsblk stats; /* we'll use this at the end */
    
    /* create the workspace for nauty to use */
    DYNALLSTAT(setword, workspace, worksize);
    DYNALLOC1(setword, workspace, worksize, 50 * m, "malloc");

    sparsegraph canon_g;
    SG_INIT(canon_g);
    
    nauty((graph*) g, lab, ptn, NULL, orbits, &options, &stats, workspace, 50 * m, m, nv, (graph*) &canon_g);

    /* THIS IS VERY IMPORTANT FOR CANONICAL STRING */
    sortlists_sg(&canon_g);
    
    char* canon_str = (char*)sgtos6(&canon_g);
    int canon_len = strlen(canon_str);
  
    printf("%s", canon_str);
    
    /* clean up */
    DYNFREE(workspace, worksize);
    DYNFREE(lab,lab_n);
    DYNFREE(ptn,ptn_n);
    DYNFREE(orbits,orbits_n);
    
    SG_FREE(canon_g);
}



/**
 * outputEColoredCanonicalLabeling takes an edge-colored 
 * graph and writes the canonical string to standard out.
 *
 * @param g the graph as a sparsegraph.
 * @param directed 0 if undirected, nonzero if directed.
 * @param binary 0 to use k-layer graph, nonzero to use binary layer graph. 
 * @param k the number of colors used (should be 0...k-1).
 * @param c the colors. c[i] should store the color of the pair ab with co-lex index i.
 */
void outputEColoredCanonicalLabeling(sparsegraph* g, int directed, int binary, int k, int* c)
{
    int nv, m;
    DYNALLSTAT(int, lab, lab_n);
    DYNALLSTAT(int, ptn, ptn_n);
    DYNALLSTAT(int, orbits, orbits_n);
  
    /* Setup of lab and ptn is in another method */
    sparsegraph* layer_g = 0;
    
    if ( binary == 0 )
    {
      nv = k * g->nv;
      m = (nv + WORDSIZE - 1) / WORDSIZE;
      nauty_check(WORDSIZE, m, nv, NAUTYVERSIONID);
      
      DYNALLOC1(int, lab, lab_n, nv, "malloc");
      DYNALLOC1(int, ptn, ptn_n, nv, "malloc");
      DYNALLOC1(int, orbits, orbits_n, nv, "malloc");
      
      layer_g = getKLayerGraph(g, k, c, lab, ptn);
    }
    else
    {
      int logkp1 = ceil(log2(k+1));
      nv = logkp1 * g->nv;
      m = (nv + WORDSIZE - 1) / WORDSIZE;
      nauty_check(WORDSIZE, m, nv, NAUTYVERSIONID);
      
      DYNALLOC1(int, lab, lab_n, nv, "malloc");
      DYNALLOC1(int, ptn, ptn_n, nv, "malloc");
      DYNALLOC1(int, orbits, orbits_n, nv, "malloc");
      
      layer_g = getBinaryLayerGraph(g, k, c, lab, ptn);
    }
      
    
    /* set the options */
    static DEFAULTOPTIONS_SPARSEGRAPH( options);
    options.defaultptn = FALSE; /* WE NEED THE COLORS! */
    options.getcanon = TRUE; /* gets labels */
    
    if ( directed == 0 )
    {
      options.digraph = FALSE;
    }
    else
    {
      options.digraph = TRUE;
    }
    
    statsblk stats; /* we'll use this at the end */
    
    /* create the workspace for nauty to use */
    DYNALLSTAT(setword, workspace, worksize);
    DYNALLOC1(setword, workspace, worksize, 50 * m, "malloc");

    sparsegraph* canon_layer_g = (sparsegraph*)malloc(sizeof(sparsegraph));
    SG_INIT((*canon_layer_g));
    
    nauty((graph*) layer_g, lab, ptn, NULL, orbits, &options, &stats, workspace, 50 * m, m, nv, (graph*) canon_layer_g);
    
    /* Now, we can't just output the string for canon_g */
    /* first, find inverse of first g->nv positions of lab */
    int* lab_inv = (int*) malloc(g->nv * sizeof(int));
    int i;
    for ( i = 0; i < g->nv; i++ )
    {
      lab_inv[lab[i]] = i;
    }
    
    /* Instead, we use the fist g->nv values of lab to make a new graph */
    sparsegraph* canon_g = (sparsegraph*)malloc(sizeof(sparsegraph));
    SG_INIT((*canon_g));
    canon_g->nv = g->nv;
    canon_g->nde = g->nde;
    
    canon_g->v = (int*)malloc(g->nv * sizeof(int));
    canon_g->d = (int*)malloc(g->nv * sizeof(int));
    canon_g->e = (int*)malloc(g->nde * sizeof(int));
    
    int cur_e_index = 0;
    int j;
    for ( i = 0; i < g->nv; i++ )
    {
      canon_g->v[i] = cur_e_index;
      canon_g->d[i] = g->d[lab[i]];
      
      for ( j = 0; j < canon_g->d[i]; j++ )
      {
	canon_g->e[cur_e_index] = lab_inv[ g->e[ g->v[lab[i]] + j] ];
	cur_e_index++;
      }
    }
    
    /* THIS IS VERY IMPORTANT FOR CANONICAL STRING */
    sortlists_sg(canon_g);
    
    char* canon_str = sgtos6(canon_g);
    int canon_len = strlen(canon_str);
  
    printf("%s", canon_str);
    
    /* clean up */
    DYNFREE(workspace, worksize);
    DYNFREE(lab,lab_n);
    DYNFREE(ptn,ptn_n);
    DYNFREE(orbits,orbits_n);
    
    free(lab_inv);
    SG_FREE((*layer_g));
    SG_FREE((*canon_g));
    SG_FREE((*canon_layer_g));
    free(layer_g);
    free(canon_g);
    free(canon_layer_g);
}


/**
 * setColoredPartition sets up the nauty data structures lab & ptn according to a 
 * given vertex coloring. After the execution, lab & ptn will be ready to send to nauty.
 *
 * @param g the graph as a sparsegraph.
 * @param k the number of colors.
 * @param c the colors as in outputEColoredCanonicalLabeling.
 * @param lab an integer array of at least g->nv positions. WILL BE MODIFIED.
 * @param ptn an integer array of at least g->nv positions. WILL BE MODIFIED.
 */
void setColoredPartition(sparsegraph* g, int k, int* c, int* lab, int* ptn)
{
  /* loop over all colos to fill it */
  int cur_index = 0;
  int i,j;
  for ( i = 0; i < k; i++ )
  {
    for ( j = 0; j < g->nv; j++ )
    {
      if ( c[j] == i )
      {
    lab[cur_index] = j;
    ptn[cur_index] = 1;
    cur_index++;
      }
    }
    
    if (cur_index > 0)
    {
      ptn[cur_index - 1] = 0;
    }
  }
}


/**
 * getKLayerGraph builds a k-layered graph and sets up the nauty data structures 
 * lab & ptn according to a given edge coloring. After the execution, lab & ptn 
 * will be ready to send to nauty. The resulting sparsegraph must be cleaned and
 * deleted with 'free()' by the caller. Further, the layers of the returned graph
 * are presented as consecutive copies of the vertices of g (layer 0, layer 1, ...).
 *
 * @param g the graph as a sparsegraph.
 * @param k the number of colors.
 * @param c the colors as in outputEColoredCanonicalLabeling.
 * @param lab an integer array of at least k * g->nv positions. WILL BE MODIFIED.
 * @param ptn an integer array of at least k * g->nv positions. WILL BE MODIFIED.
 * @return a new sparsegraph to send to nauty. Stores the layered graph.
 */
sparsegraph* getKLayerGraph(sparsegraph* g, int k, int* c, int* lab, int* ptn)
{
  sparsegraph* layer_g = (sparsegraph*)malloc(sizeof(sparsegraph));
  SG_INIT((*layer_g));
  layer_g->nv = k * g->nv;
  layer_g->nde = 2 * k * g->nv + g->nde;
  
  layer_g->v = (int*)malloc(layer_g->nv*sizeof(int));
  layer_g->d = (int*)malloc(layer_g->nv*sizeof(int));
  layer_g->e = (int*)malloc(layer_g->nde*sizeof(int));
  
  int cur_v_index = 0;
  int cur_e_index = 0;
  int i,j,a;
  /* for all colors */
  for ( i = 0; i < k; i++ )
  {
    /* for all vertices */
    for ( j = 0; j < g->nv; j++ )
    {
      lab[cur_v_index] = cur_v_index;
      ptn[cur_v_index] = 1;
      
      layer_g->v[cur_v_index] = cur_e_index;
      layer_g->d[cur_v_index] = 2;
      
      layer_g->e[cur_e_index] = g->nv * ((i + 1) % k) + j;
      layer_g->e[cur_e_index+1] = g->nv * ((i + k - 1) % k) + j;
      
      cur_e_index += 2;
      
      for ( a = 0; a < g->d[j]; a++ )
      {
	int otherv = g->e[g->v[j]+a];
	int colex = indexOf(j, otherv);
	
	if ( c[colex] == i )
	{
	  layer_g->e[cur_e_index] = k * i + otherv;
	  layer_g->d[cur_v_index] = layer_g->d[cur_v_index] + 1;
	  cur_e_index++;
	}
      }
	  
      cur_v_index++;
    }
    
    /* close the partition */
    ptn[cur_v_index - 1] = 0;
  }
  
  return layer_g;
}


/**
 * getBinaryLayerGraph builds a binary layered graph and sets up the nauty data structures 
 * lab & ptn according to a given edge coloring. After the execution, lab & ptn 
 * will be ready to send to nauty. The resulting sparsegraph must be cleaned and
 * deleted with 'free()' by the caller. Further, the layers of the returned graph
 * are presented as consecutive copies of the vertices of g (layer 0, layer 1, ...).
 *
 * @param g the graph as a sparsegraph.
 * @param k the number of colors.
 * @param c the colors as in outputEColoredCanonicalLabeling.
 * @param lab an integer array of at least log(k+1) * g->nv positions. WILL BE MODIFIED.
 * @param ptn an integer array of at least log(k+1) * g->nv positions. WILL BE MODIFIED.
 * @return a new sparsegraph to send to nauty. Stores the layered graph.
 */
sparsegraph* getBinaryLayerGraph(sparsegraph* g, int k, int* c, int* lab, int* ptn)
{
  int logkp1 = (int)ceil(log2(k+1));
  sparsegraph* layer_g = (sparsegraph*)malloc(sizeof(sparsegraph));
  SG_INIT((*layer_g));
  
  layer_g->nv = logkp1*g->nv;
  layer_g->nde = 2*(logkp1)*g->nv + logkp1 * g->nde; /* this is only an upper bound for now */
  
  layer_g->v = (int*)malloc(layer_g->nv * sizeof(int));
  layer_g->d = (int*)malloc(layer_g->nv * sizeof(int));
  layer_g->e = (int*)malloc(layer_g->nde * sizeof(int));
  
  int cur_v_index = 0;
  int cur_e_index = 0;
  int i,j,a;
  
  /* for all layers */
  for ( i = 0; i < logkp1; i++ )
  {
    /* for all vertices */
    for ( j = 0; j < g->nv; j++ )
    {
      lab[cur_v_index] = cur_v_index;
      ptn[cur_v_index] = 1;
      
      layer_g->v[cur_v_index] = cur_e_index;
      layer_g->d[cur_v_index] = 2;
      
      layer_g->e[cur_e_index] = g->nv * ((i + 1) % logkp1) + j;
      layer_g->e[cur_e_index+1] = g->nv * ((i + logkp1 - 1) % logkp1) + j;
      
      cur_e_index += 2;
      
      for ( a = 0; a < g->d[j]; a++ )
      {
	int otherv = g->e[g->v[j]+a];
	int colex = indexOf(j, otherv);
	
	int colorbits = c[colex] + 1;
	
	if ( (colorbits & (1 << i)) != 0 )
	{
	  /* this layer must include this edge! */
	  layer_g->e[cur_e_index] = k * i + otherv;
	  layer_g->d[cur_v_index] = layer_g->d[cur_v_index] + 1;
	  cur_e_index++;
	}
      }
      
      cur_v_index++;
    }
    
    /* close the partition */
    ptn[cur_v_index - 1] = 0;
  }
  
  layer_g->nde = cur_e_index;
  
  return layer_g;
}

/**
 * indexOf
 *
 * The co-lex index of the pair (i,j).
 */
int indexOf(int i, int j)
{
  if ( i == j )
    {
        /* bad input */
        return -1;
    }

    if ( j < i )
    {
        /* wrong order */
        return indexOf(j, i);
    }

    /* CO-LEX ORDER */

    /* THERE ARE (j choose 2) SETS WITH NUMBERS STRICTLY LESS THAN j */
    return ((j*(j-1))/2) + i;
}


#ifdef USE_MAIN

/**
 * the test executable
 *
 * Argument -u says graphs are undirected.
 * Argument -d says graphs are directed.
 * Argument -v says use a vertex coloring.
 * Argument -e says use an edge coloring.
 * Argument -b says use the binary labeled graph.
 *
 * Input (via stdin):
 * sparse6 representation of a graph
 * n colors for the vertices (if -v)
 * An integer M for # of colored edges, followed by M triples x y i where the edge xy has color i.
 * 
 */
int main(int argc, char** argv)
{
  char buffer[1500];
  int use_vertex_coloring = 0;
  int use_edge_coloring = 0;
  int directed = 1; /* for safety. May slow slightly in case of undirected */
  int binary = 0;
  
  int i;
  for ( i = 1; i < argc; i++ )
  {
    if ( strcmp(argv[i], "-u") == 0 )
    {
      directed = 0;
    }
    if ( strcmp(argv[i], "-b") == 0 )
    {
      binary = 1;
    }
    if ( strcmp(argv[i], "-d") == 0 )
    {
      directed = 1;
    }
    if ( strcmp(argv[i], "-v") == 0 )
    {
      use_vertex_coloring = 1;
    }
    else if (strcmp(argv[i], "-e") == 0 )
    {
      use_edge_coloring = 1; 
    }
  }
  
  while ( scanf("%s", buffer) != EOF )
  {
    /* buffer holds an s6 string */
    sparsegraph* g = (sparsegraph*)malloc(sizeof(sparsegraph));
    SG_INIT((*g));
     
    int num_loops;
    stringtosparsegraph(buffer, g, &num_loops);
    
    int k = 1;
    int* c = 0;
    int* cp = 0;
    if ( use_vertex_coloring > 0 )
    {
      c = (int*)malloc(g->nv * sizeof(int));
      int i;
      for ( i = 0; i < g->nv; i++ )
      {
	int color;
	scanf("%d", &color);
	c[i] = color;
	
	if ( color >= k )
	{
	  k = color + 1;
	}
      }
    }
    
    if ( use_edge_coloring > 0 )
    {
      cp = (int*)malloc(g->nv * g->nv * sizeof(int));
      bzero(cp, g->nv * g->nv * sizeof(int));
      int num_edges = 0;
      scanf("%d", &num_edges);
      int i;
      for ( i = 0; i < num_edges; i++ )
      {
	int x,y;
	int color;
	scanf("%d %d %d", &x, &y, &color);
	cp[indexOf(x,y)] = color;
	
	if ( color >= k )
	{
	  k = color + 1;
	}
      }
    }
    
    if ( use_vertex_coloring == 0 )
    {
      if ( use_edge_coloring == 0 )
      {
	  outputCanonicalLabeling(g, directed);
      }
      else
      {
	  outputEColoredCanonicalLabeling(g, directed, binary, k, cp);
      }
    }
    else
    {
      if ( use_edge_coloring == 0 )
      {
	outputVColoredCanonicalLabeling(g, directed, k, c);
      }
      else
      {
	printf("Totally-colored graphs are not currently supported!\n");
	exit(1);
      }
    }
    
    SG_FREE((*g));
    free(g);
  }
  
  return 0;
}

#endif